/*
 * stm32H743custom.cpp
 *
 *  Created on: 10 Feb 2020
 *      Author: Erick Cabral
 */

#include"stm32H743custom.h"

GPIOx::GPIOx(uint32_t gpio_baseAddress) {
	this->gpio_x_baseAddress = gpio_baseAddress;
	this->pMODER = (uint32_t*) (this->gpio_x_baseAddress + 0x00U); //OFFSET -> 0x00
	this->pTYPE = (uint32_t*) (this->gpio_x_baseAddress + 0x04U); //OFFSET -> 0x04
	this->pSPEED = (uint32_t*) (this->gpio_x_baseAddress + 0x08U); //OFFSET -> 0x08
	this->pPUPD = (uint32_t*) (this->gpio_x_baseAddress + 0x0CU); //OFFSET -> 0x0C
	this->pIDR = (uint32_t*) (this->gpio_x_baseAddress + 0x10U); //OFFSET -> 0x10
	this->pODR = (uint32_t*) (this->gpio_x_baseAddress + 0x14U); //OFFSET -> 0x14
	this->pBSRR = (uint32_t*) (this->gpio_x_baseAddress + 0x18U); //OFFSET -> 0x18
	this->pLCKR = (uint32_t*) (this->gpio_x_baseAddress + 0x1CU); //OFFSET -> 0x1C
	this->pAFRL = (uint32_t*) (this->gpio_x_baseAddress + 0x20U); //OFFSET -> 0x20
	this->pAFRH = (uint32_t*) (this->gpio_x_baseAddress + 0x24U); //OFFSET -> 0x24
}

void GPIOx::setGPIOx_MODE(uint8_t portValue) {
	*(this->pMODER) = portValue;
}

void GPIOx::setPINx_MODE(uint8_t pinNumber, uint8_t mode) {
	setTwoBitsRegister(this->pMODER, pinNumber, mode);

}

void GPIOx::setPINx_TYPE(uint8_t pinNumber, uint8_t mode) {
	setTwoBitsRegister(this->pTYPE, pinNumber, mode);

}

void GPIOx::setPINx_SPEED(uint8_t pinNumber, uint8_t mode) {
	setTwoBitsRegister(this->pSPEED, pinNumber, mode);

}

void GPIOx::setPINx_PUPD(uint8_t pinNumber, uint8_t mode) {
	setTwoBitsRegister(this->pPUPD, pinNumber, mode);

}

uint8_t GPIOx::PINx_IDR(uint8_t pinNumber, uint8_t mode) {
	//read pin
	return 0;
}

void GPIOx::setPINx_ODR(uint8_t pinNumber, uint8_t mode) {
	setOneBitRegister(this->pODR, pinNumber, mode);
}


//====== SUPPORT FUNCTIONS ======== //

void setTwoBitsRegister(volatile uint32_t *registerToSet, uint8_t bitToSet,
		uint8_t valueToSet) {
	uint32_t bit1 = 0;
	switch (valueToSet) {
	case 0:
		*registerToSet &= ~(1 << (2 * bitToSet + 1) | 1 << (2 * bitToSet));
		bit1 |= (0 << (2 * bitToSet + 1) | 0 << (2 * bitToSet));
		break;
	case 1:
		*registerToSet &= ~(1 << (2 * bitToSet + 1) | 1 << (2 * bitToSet));
		bit1 |= (0 << (2 * bitToSet + 1) | 1 << (2 * bitToSet));
		break;
	case 2:
		*registerToSet &= ~(1 << (2 * bitToSet + 1) | 1 << (2 * bitToSet));
		bit1 |= (1 << (2 * bitToSet + 1) | 0 << (2 * bitToSet));
		break;
	case 3:
		*registerToSet &= ~(1 << (2 * bitToSet + 1) | 1 << (2 * bitToSet));
		bit1 |= (1 << (2 * bitToSet + 1) | 1 << (2 * bitToSet));
		break;
	}
	*registerToSet |= bit1;
}
;

void setOneBitRegister(volatile uint32_t *registerToSet, uint8_t bitToSet,
		uint8_t valueToSet) {
	uint32_t bit1 = 0;
	*registerToSet &= ~(1 << (bitToSet));
	bit1 |= (valueToSet << (bitToSet));
	*registerToSet |= bit1;
}
;
