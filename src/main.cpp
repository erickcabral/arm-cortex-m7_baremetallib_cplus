/*
 * main.cpp
 *
 *  Created on: 10 Feb 2020
 *      Author: Erick Cabral
 */

#include "stm32H743custom.h"
int main(void){

	RCC rcc(RCC_BASEADDRESS);
	rcc.setABH4ENR(2);

	GPIOx gpioB(GPIOB_BASEADDRES);
	//gpioB.setGPIOx_MODE(0x00000004);
	gpioB.setPINx_MODE(PIN_0, MODE_OUTPUT);
	gpioB.setPINx_TYPE(PIN_0, TYPE_PUSH_PULL);
	gpioB.setPINx_PUPD(PIN_0, PUPD_UP);
	gpioB.setPINx_ODR(PIN_0, HIGH);

	gpioB.setPINx_MODE(PIN_7, MODE_OUTPUT);
	gpioB.setPINx_TYPE(PIN_7, TYPE_PUSH_PULL);
	gpioB.setPINx_PUPD(PIN_7, PUPD_UP);
	gpioB.setPINx_ODR(PIN_7, HIGH);

	gpioB.setPINx_MODE(PIN_14, MODE_OUTPUT);
	gpioB.setPINx_TYPE(PIN_14, TYPE_PUSH_PULL);
	gpioB.setPINx_PUPD(PIN_14, PUPD_UP);
	gpioB.setPINx_ODR(PIN_14, HIGH);

	while(1){

	}
}


